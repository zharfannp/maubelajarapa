import { useState } from "react";
import {
  Typography,
  TextField,
  Stack,
  IconButton,
  Button,
  Box,
  Grid,
} from "@mui/material";
import BorderColorIcon from "@mui/icons-material/BorderColor";
import AddIcon from "@mui/icons-material/Add";
import { reorder } from "../helper";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import LessonList from "./lesson-list";
import { ButtonAdd } from "../pages/event.styles";

const SessionListItem = ({ item, index }) => {
  const [nameSession, setNameSession] = useState(`${item.title}`);
  const [show, setShow] = useState(false);
  const [newLesson, setNewLesson] = useState(item.lesson);

  const addLesson = () => {
    let obj = {};

    newLesson.map(
      (item, index) => (obj = { ...item, id: newLesson.length + 1 })
    );

    setNewLesson([...newLesson, obj]);
  };

  const onDragEnd = ({ destination, source, type }) => {
    if (!destination) return;

    if (destination.index === source.index) {
      return;
    }

    if (type === "LESSON") {
      const newItems = reorder(newLesson, source.index, destination.index);

      setNewLesson(newItems);
    }
  };

  const showTitle = (e) => {
    if (e.keyCode === 13) {
      e.preventDefault();
      setShow(false);
    }
  };

  return (
    <Stack spacing={2}>
      <Grid container spacing={2}>
        <Grid
          item
          xs={6}
          sx={{
            display: "flex",
            alignItems: "center",
            marginTop: "-5rem",
            marginLeft: "3rem",
          }}
        >
          <Stack direction="row" spacing={2}>
            {show ? (
              <TextField
                value={nameSession}
                onChange={(e) => setNameSession(e.target.value)}
                onKeyDown={(e) => showTitle(e)}
                autoFocus={true}
              />
            ) : (
              <Typography variant="h6">{nameSession}</Typography>
            )}
            <IconButton
              size="small"
              edge="start"
              sx={{ color: "#BCC0D0" }}
              aria-label="menu"
              onClick={() => setShow(true)}
            >
              <BorderColorIcon />
            </IconButton>
          </Stack>
        </Grid>
      </Grid>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="list-lesson" type="LESSON">
          {(provided) => (
            <div ref={provided.innerRef} {...provided.droppableProps}>
              <LessonList newLesson={newLesson} />
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
      <Grid container sx={{ display: "flex", justifyContent: "start" }}>
        <ButtonAdd
          variant="contained"
          startIcon={<AddIcon />}
          onClick={addLesson}
        >
          Add Lesson Material
        </ButtonAdd>
      </Grid>
    </Stack>
  );
};

export default SessionListItem;
