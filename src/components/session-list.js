import { useEffect, useState } from "react";
import SessionListItem from "./session-list-item";
import { Draggable } from "react-beautiful-dnd";
import { CardSession } from "../pages/event.styles";
import {
  Box,
  Card,
  CardHeader,
  CardAction,
  CardContent,
  IconButton,
  Icon,
  Avatar,
  TextField,
  Typography,
} from "@mui/material";
import DragIndicatorIcon from "@mui/icons-material/DragIndicator";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import DeleteIcon from "@mui/icons-material/Delete";
import DialogDelete from "./dialog-delete";

const SessionList = ({ session }) => {
  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleDelete = (id) => {
    // const arr = session.filter((item) => item.id !== value);
  };

  return session.map((item, index) => (
    <Draggable key={item.id} draggableId={`Session-${item.id}`} index={index}>
      {(provided) => (
        <CardSession
          sx={{ border: 1, borderColor: "divider", marginBottom: "1rem" }}
          ref={provided.innerRef}
          {...provided.draggableProps}
        >
          <CardHeader
            avatar={
              <IconButton {...provided.dragHandleProps}>
                <DragIndicatorIcon />
              </IconButton>
            }
            action={
              <IconButton aria-label="settings">
                <DeleteIcon onClick={() => setOpen(true)} />
                <DialogDelete
                  open={open}
                  handleClose={handleClose}
                  item={item.id}
                  handleDelete={handleDelete}
                />
              </IconButton>
            }
          />
          <CardContent>
            <SessionListItem item={item} index={index} key={item.id} />
          </CardContent>
        </CardSession>
      )}
    </Draggable>
  ));
};

export default SessionList;
