import { Box, Toolbar, IconButton } from "@mui/material";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { StyledNavbar, StyledTypography } from '../pages/event.styles';

const Navbar = () => {
	return ( 
		<Box sx={{ flexGrow: 1 }}>
      <StyledNavbar position="static">
        <Toolbar>
					<IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ 
							mr: 2,
							color: '#333' 
						}}
          >
            <ArrowBackIcon />
          </IconButton>
          <StyledTypography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Event
          </StyledTypography>
        </Toolbar>
      </StyledNavbar>
    </Box>
	);
}
 
export default Navbar;