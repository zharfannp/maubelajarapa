import { List, ListItem } from "@mui/material";
import LessonListItem from "./lesson-list-item";

const LessonList = ({ newLesson }) => {
  return newLesson.map((item, index) => (
    <List>
      <ListItem>
        <LessonListItem item={item} index={index} key={item.id} />
      </ListItem>
    </List>
  ));
};

export default LessonList;
