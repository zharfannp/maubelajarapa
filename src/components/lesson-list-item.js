import {
  Typography,
  Stack,
  IconButton,
  Box,
  ListItemIcon,
  Grid,
  Divider,
} from "@mui/material";
import DragIndicatorIcon from "@mui/icons-material/DragIndicator";
import VideocamOutlinedIcon from "@mui/icons-material/VideocamOutlined";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import FileDownloadOutlinedIcon from "@mui/icons-material/FileDownloadOutlined";
import { Draggable } from "react-beautiful-dnd";

const LessonListItem = ({ item, index }) => {
  return (
    <Draggable key={item.id} draggableId={`Lesson-${item.id}`} index={index}>
      {(provided) => (
        <Grid
          container
          spacing={2}
          ref={provided.innerRef}
          {...provided.draggableProps}
          sx={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Grid item xs={6}>
            <Stack direction="row" spacing={1}>
              <ListItemIcon>
                <IconButton
                  {...provided.dragHandleProps}
                  sx={{ marginTop: "-0.5rem" }}
                >
                  <DragIndicatorIcon />
                </IconButton>
                <VideocamOutlinedIcon />
              </ListItemIcon>
              <Typography variant="subtitle1">{item.lessonTitle}</Typography>
              <Divider orientation="vertical" flexItem />
              <Typography variant="subtitle2" sx={{ color: "blue" }}>
                Required
              </Typography>
              <Typography variant="subtitle2" color="text.secondary">
                Previewable
              </Typography>
            </Stack>
          </Grid>
          <Grid item xs={6}>
            <Stack direction="row">
              <Box sx={{ display: "flex", marginRight: "4rem" }}>
                <AccessTimeIcon /> &nbsp;
                {item.date}
              </Box>
              <Box sx={{ display: "flex", marginRight: "4rem" }}>
                <AccessTimeIcon /> &nbsp;
                {item.duration}
              </Box>
              <Box sx={{ display: "flex", marginRight: "4rem" }}>
                <FileDownloadOutlinedIcon /> &nbsp; Downloadble
              </Box>
            </Stack>
          </Grid>
        </Grid>
      )}
    </Draggable>
  );
};

export default LessonListItem;
