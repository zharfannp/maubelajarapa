import Navbar from "./components/navbar";
import Event from "./pages/event";
import { Container } from "./pages/event.styles";

function App() {
  return (
    <div className="App">
      <Navbar/>
      <Container>
        <Event/>
      </Container>
    </div>
  );
}

export default App;
