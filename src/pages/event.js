import { useState } from "react";
import { Box, Typography, Stack, Button, Grid, Tab, Tabs } from "@mui/material";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { ContainerCurricullum, ButtonAdd } from "./event.styles";
import SessionList from "../components/session-list";
import AddIcon from "@mui/icons-material/Add";
import { reorder } from "../helper";
import { DragDropContext, Droppable } from "react-beautiful-dnd";

const Event = () => {
  const [session, setSession] = useState([
    {
      id: 1,
      title: "Session 1",
      lesson: [
        {
          id: 1,
          lessonTitle: "Video Masak",
          date: "24 Oktober 2021, 16:30",
          duration: "06:30 Min",
        },
      ],
    },
  ]);

  const addSession = () => {
    let obj = {};

    session.map(
      (item, index) =>
        (obj = {
          ...item,
          id: session.length + 1,
          title: `Session ${session.length + 1}`,
          lesson: [
            {
              id: 1,
              lessonTitle: "Video Masak",
              date: "24 Oktober 2021, 16:30",
              duration: "06:30 Min",
            },
          ],
        })
    );

    setSession([...session, obj]);
  };

  const onDragEnd = ({ destination, source, type }) => {
    if (!destination) return;

    if (destination.index === source.index) {
      return;
    }

    if (type === "SESSION") {
      const newItems = reorder(session, source.index, destination.index);

      setSession(newItems);
    }
  };

  return (
    <Stack spacing={3}>
      <Grid container spacing={1}>
        <Grid item xs={10}>
          <Stack
            direction="row"
            spacing={4}
            sx={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <Typography variant="h4">
              Belajar dan praktek cinematic videography
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Last edited 18 October 2021 | 13:23
            </Typography>
          </Stack>
        </Grid>
        <Grid
          item
          xs={2}
          sx={{
            display: "flex",
            justifyContent: "end",
          }}
        >
          <Button
            variant="outlined"
            startIcon={<VisibilityIcon />}
            sx={{ borderRadius: "8px" }}
          >
            Preview
          </Button>
        </Grid>
      </Grid>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs value={0}>
          <Tab label="Curricullum" />
        </Tabs>
      </Box>
      <ContainerCurricullum sx={{ border: 1, borderColor: "divider" }}>
        <Typography variant="body1">
          Event Schedule: 24 Oktober 2021, 16:30
        </Typography>
      </ContainerCurricullum>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="list-session" type="SESSION">
          {(provided) => (
            <div ref={provided.innerRef} {...provided.droppableProps}>
              <SessionList session={session} />
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
      <Grid container sx={{ display: "flex", justifyContent: "end" }}>
        <ButtonAdd
          variant="contained"
          startIcon={<AddIcon />}
          onClick={addSession}
        >
          Add Session
        </ButtonAdd>
      </Grid>
    </Stack>
  );
};

export default Event;
