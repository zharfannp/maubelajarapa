import { AppBar, styled, Typography, Box, Card, Button } from "@mui/material";

export const StyledNavbar = styled(AppBar)`
  background-color: #fff;
`;

export const StyledTypography = styled(Typography)`
  color: #333;
`;

export const Container = styled(Box)`
  padding: 48px;
`;

export const ContainerCurricullum = styled(Box)`
  padding: 24px;
  border-radius: 10px;
`;

export const CardSession = styled(Card)`
  border-radius: 10px;
`;

export const ButtonAdd = styled(Button)`
  background-color: #7800ef;
  border-radius: 8px;
`;
